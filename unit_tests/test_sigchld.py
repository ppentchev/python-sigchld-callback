"""
Test various aspects of the ChildCallback class.
"""

# Copyright (c) 2018  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


import errno
import os
import signal
import subprocess
import sys
import unittest

from typing import Callable, Dict, List, Tuple, Type

import mock


from sigchld_callback import sigchld_callback


_TYPING_USED = (Callable, Dict, List, Tuple, Type)


TEST_DATA = {
    'simple': [(1, 0), (2, 3)],
}


def fake_waitpid(lst):
    # type: (List[Tuple[int, int]]) -> Callable[[int, int], Tuple[int, int]]
    """
    Return a function that emulates os.waitpid() and returns tuples from
    the specified list on consecutive invocations.
    """
    waitpid_iter = iter(lst)

    def fake_fake_waitpid(_pid, _flags):  # type: (int, int) -> Tuple[int, int]
        """
        Return tuples from the specified list and ECHILD at the end.
        """
        assert _pid == -1
        assert _flags == os.WNOHANG
        try:
            return next(waitpid_iter)
        except StopIteration:
            raise OSError(errno.ECHILD, 'No more child processes')

    return fake_fake_waitpid


class TestCallbacks(unittest.TestCase):
    """
    Test the initialization of the signal handler.
    """

    @mock.patch('sigchld_callback.sigchld_callback.ChildCallback._old_handler')
    @mock.patch('sigchld_callback.sigchld_callback.ChildCallback._callbacks',
                new=list())
    @mock.patch('signal.signal')
    @mock.patch('os.waitpid', new=fake_waitpid(TEST_DATA['simple']))
    def test_already_initialized(self, signal_signal, _old_handler):
        # type: (TestCallbacks, mock.MagicMock, mock.MagicMock) -> None
        """
        Test the operation of ChildCallback when the signal handler has
        already been initialized (no need to call signal.signal()).
        """
        cb_res = []  # type: List[List[Tuple[int, int]]]
        cb_order = []  # type: List[int]

        def get_callback(callback_index):
            # type: (int) -> sigchld_callback.CallbackType
            """
            Return a callback function for the specified index.
            """
            def real_callback(proc_info):
                # type: (List[Tuple[int, int]]) -> None
                """
                The returned callback function: modify the specified element
                of the results array and store the index into the order array.
                """
                cb_res[callback_index].extend(proc_info)
                cb_order.append(callback_index)

            return real_callback

        testee = sigchld_callback.ChildCallback

        for idx in range(3):
            cb_res.append([])
            testee.register_callback(get_callback(idx))
            self.assertEqual(signal_signal.call_count, 0)
            self.assertEqual(cb_res, [[] for _ in range(idx + 1)])

        # Can't avoid accessing a protected member here, can we...
        # pylint: disable=protected-access
        testee._sigchld_handler(signal.SIGCHLD, sys._getframe(0))
        for idx in range(3):
            self.assertEqual(cb_res[idx], TEST_DATA['simple'])
        self.assertEqual(cb_order, list(range(3)))


class TestReal(unittest.TestCase):
    """
    Run a real test with real child processes.
    """

    def test_real(self):
        # type: (TestReal) -> None
        """
        Run a real test with two child processes.
        """
        received = []  # type: List[Tuple[int, int]]

        def cb_store(proc_info):
            # type: (List[Tuple[int, int]]) -> None
            """
            Store information about completed processes.
            """
            received.extend(proc_info)

        sigchld_callback.ChildCallback.register_callback(cb_store)

        for _ in range(10):
            self.assertEqual(len(received), 0)

            p_true = subprocess.Popen(['true'],
                                      stdin=subprocess.PIPE,
                                      stdout=subprocess.PIPE,
                                      shell=False)
            p_false = subprocess.Popen(['false'],
                                       stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE,
                                       shell=False)
            p_true.communicate()
            p_false.communicate()

            data = {}  # type: Dict[int, int]
            while received:
                proc = received.pop()
                data[proc[0]] = proc[1]

            self.assertEqual(len(data), 2)
            self.assertEqual(data[p_true.pid], 0)
            self.assertNotEqual(data[p_false.pid], 0)
