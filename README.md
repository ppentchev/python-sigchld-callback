# The common framework for handling SIGCHLD signals

The `sigchld_callback` module provides a common framework for catching
SIGCHLD signals from terminated child processes and sending the information
about them to interested parties via registered callbacks.

## The ChildCallback class

The `sigchld_callback` module provides the `ChildCallback` class which
has the following static methods:

* `register_callback(callback)`: add a callback function to the list of
  functions to invoke when SIGCHLD is received
* `unregister_callback(callback)`: remove a function from the list

The callback functions are called with a single parameter: a list of
`(pid, exitcode)` tuples.  Thus, a sample callback function that waits
for a previously spawned process to exit would look like this:

    def sigchld_callback(proc_info):
        # type: (List[Tuple[int, int]]) -> None
        for proc in proc_info:
            if proc[0] == child_pid:
                child_exitcode = proc[1]
                break

The `sigchld_callback` module is fully typed.

## Author

Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>

## License

    Copyright (c) 2018  Peter Pentchev <roam@ringlet.net>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.
