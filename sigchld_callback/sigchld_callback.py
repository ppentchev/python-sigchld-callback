"""
A common framework for catching SIGCHLD signals from terminated
child processes and sending the information about them to
interested parties via registered callbacks.
"""

# Copyright (c) 2018  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


import errno
import os
import signal
import sys
import types

from typing import Callable, List, Tuple, Type, TypeVar, Union


_TYPING_USED = (Callable, List, Tuple, Type, TypeVar, Union)


if sys.version_info[0] < 3:
    SignalType = int
    SignalHandlers = int
else:
    SignalType = signal.Signals
    SignalHandlers = signal.Handlers


SignalHandlerType = Union[
    Callable[
        [SignalType, types.FrameType],  # pylint: disable=no-member
        None
    ],
    int,
    SignalHandlers,  # pylint: disable=no-member
    None,
]
CallbackType = Callable[
    [List[Tuple[int, int]]],
    None
]


def _sentinel_handler_not_set(_sig, _frame):
    # type: (SignalType, types.FrameType) -> None
    pass


class ChildCallback(object):
    """
    A class providing a singleton object for registering callbacks
    that will be invoked when a SIGCHLD signal is received.
    """

    _old_handler = _sentinel_handler_not_set  # type: SignalHandlerType
    _callbacks = []  # type: List[CallbackType]

    @classmethod
    def register_callback(cls, callback):
        # type: (Type[ChildCallback], CallbackType) -> None
        """
        Initialize the SIGCHLD handler if necessary and add the specified
        callback to the list of things to be invoked.
        """
        if callback in cls._callbacks:
            return
        cls._callbacks.append(callback)

        if cls._old_handler is _sentinel_handler_not_set:
            cls._old_handler = signal.signal(signal.SIGCHLD,
                                             cls._sigchld_handler)

    @classmethod
    def unregister_callback(cls, callback):
        # type: (Type[ChildCallback], CallbackType) -> None
        """
        Remove the specified callback from the list of things to be invoked.
        """
        try:
            cls._callbacks.remove(callback)
        except ValueError:
            pass

    @classmethod
    def _sigchld_handler(cls, _signum, _frame):
        # type: (Type[ChildCallback], SignalType, types.FrameType) -> None
        """
        Handle SIGCHLD: wait for all the child processes that have finished
        and invoke all the callbacks.
        """
        res = []
        try:
            while True:
                pid, stat = os.waitpid(-1, os.WNOHANG)
                if pid == 0 and stat == 0:
                    break
                res.append((pid, stat))
        except OSError as err:
            if err.errno != errno.ECHILD:
                raise

        if res:
            for callback in list(cls._callbacks):
                callback(res)
